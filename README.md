# s-block


> Excellence only comes when you do something that truly resonates with your soul.

**Welcome to this part of project. Now you are going to maintain the backbone of main project.**

**Objective** : To make a program which can recognize a known person and returns the unique ID in a spreadsheet.

**Agenda** : 
-  Decide best algorithm for *real time face recognition*.
-  Learn that algorithm from basic to advance.
-  Implement the same algorithm on smaller things (e.g Recognize a cat).
-  Now implement the algorithm on a definite dataset.



**To know the initial workflow of s-block** [click here.](https://gitlab.com/Master-Ajeet/s-block/-/blob/master/Extras/s-block-converted.pdf)




## How to complete the Module?
Learning a skill happes in 3 stages Understand, Summarize & Practice (USP). This applies to the module as well.

| Process Stage | Description                                                                        | Links      |
|---------------|------------------------------------------------------------------------------------|------------|
| Understand    | Read course material and try to understand topics covered in the Module.           | [Wiki](https://gitlab.com/Master-Ajeet/s-block/-/wikis/home)   |
| Summarize (Assignment) S1,S2 etc    | Reinforce critical parts of the subject by writing them down in concise Summaries. | [Issues]() |
| Practice  (Assignment) P1,P2 etc   | Practice skills, to make your learning. This where your brain learns the most.     | [Issues]() |






